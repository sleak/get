#!/usr/bin/env python

# insertnids.py
# takes an exported job table and turns all the nids into json with the 'nid' prefix
# usage:   ./insertnids.py <exportfile>
# creates a new file called 

  
def expand_nidlist(nlist):
    """ translate a nodelist like 'nid[02516-02575,02580-02635,02836]' into a 
        json dump of explicitly-named nodes, eg '["nid02516","nid02517", ...]'
    """
    nodes = []
    prefix, sep0, nl = nlist.partition('[')
    if sep0:
        for component in nl.rstrip(']').split(','): 
            first,sep1,last = component.partition('-')
            if sep1:
                nodes += [ 'nid{:05d}'.format(i) for i in range(int(first),int(last)+1) ]
            else:
                nodes += [ 'nid{:05d}'.format(int(first)) ]
    else:
        nodes += [ prefix ]
    return json.dumps(nodes)
    
def expand_numlist(nlist):
    """ translate a nodelist like '02516-02575,02580-02635,02836' into a 
        json dump of explicitly-named nodes, eg '["nid02516","nid02517", ...]'
    """
    nodes = []
    for component in nlist.lstrip('\'[').rstrip(']\'').split(','):
        first,sep,last = component.partition('-')
        if sep:
            nodes += [ 'nid{:05d}'.format(i) for i in range(int(first),int(last)+1)]
        else:
            nodes += [ 'nid{:05d}'.format(int(first)) ]
    return json.dumps(nodes)


import json
import sys
import re

filepath = sys.argv[1]
pattern = r"(INSERT INTO table VALUES\()('[^']*'),('[^']*'),('[^']*'),([^)]*)\);"
#pattern = r"INSERT INTO table VALUES\(('[^']*').*\);"

with open(filepath) as infile:
    outfile = open('out.sql', 'w+')
    for line in infile:
        #use a regex and replace
        match = re.match(pattern, line)
        nidlist = match.group(4)
        if 'nid' in nidlist:
            components = expand_nidlist(nidlist)
        else:
            components = expand_numlist(nidlist)
        outfile.write('INSERT INTO jobs VALUES(' + match.group(2) + ',' + match.group(3) + ',\'' + components + '\',' + match.group(5) + ');\n')
    outfile.close()