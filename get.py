#!/usr/bin/env python

# get.py, a script to accept a user query of HPC annotations relating to logfiles. Intended to be integrated into LogSet.
# Note: this script assumes the user has full permissions on the database. It would be insecure for use by untrusted users (e.g., on the open web).

import argparse
import sqlite3
import json
import datetime
from dateutil import parser as dateparser
import re
import logging

#defaults
annodb = 'annotations.sqlite3'
jobdb = 'jobs.sqlite3'

try:
    config=open('anno.conf', 'r')
    for line in config:
        m = re.match('anno_db_path=\'([^=]*)\'', line)
        if m:
            annodb = m.group(1)
        m = re.match('job_db_path=\'([^=]*)\'', line)
        if m:
            jobdb = m.group(1)
    
except IOError:
    print('No anno.conf file found. Using defaults.')


def expand_nidlist(nlist):
    """ translate a nodelist like 'nid[02516-02575,02580-02635,02836]' into a 
        json dump of explicitly-named nodes, eg '["nid02516","nid02517", ...]'
    """
    nodes = []
    prefix, sep0, nl = nlist.partition('[')
    if sep0:
        for component in nl.rstrip(']').split(','): 
            first,sep1,last = component.partition('-')
            if sep1:
                nodes += [ 'nid{:05d}'.format(i) for i in range(int(first),int(last)+1) ]
            else:
                nodes += [ 'nid{:05d}'.format(int(first)) ]
    else:
        nodes += [ prefix ]
    return json.dumps(nodes)
    
def expand_numlist(nlist):
    """ translate a nodelist like '02516-02575,02580-02635,02836' into a 
        json dump of explicitly-named nodes, eg '["nid02516","nid02517", ...]'
    """
    nodes = []
    for component in nlist.lstrip('\'[').rstrip(']\'').split(','):
        first,sep,last = component.partition('-')
        if sep:
            nodes += [ 'nid{:05d}'.format(i) for i in range(int(first),int(last)+1)]
        else:
            nodes += [ 'nid{:05d}'.format(int(first)) ]
    return json.dumps(nodes)


def filter_anns(componentlist, annotations):
# filter annotations to keep only annotations for the components in the list
    filtered_anns = []
    for ann in annotations:
        if ann[14] != None:
            comps_in_ann = json.loads(ann[14])
            for component in componentlist:
                if component in comps_in_ann:
                    filtered_anns.append(ann)
                    break
    return filtered_anns


def get_parents(component, depth):
    #hit the db
    arch_connn = sqlite3.connect(annodb)
    arch_cur = arch_connn.cursor()
    arch_cur.execute("select parent from physicalcomponents where cname='" + component + "'")
    parents = []
    depth-=1
    result = arch_cur.fetchone()
    if result != None:
        result_parent = result[0]
        if result_parent != None:
            parents.append(result_parent)
            if depth > 0:
                parents += get_parents(result_parent, depth)
    return parents


def get_children(component, depth):
    #hit the db
    arch_connn = sqlite3.connect(annodb)
    arch_cur = arch_connn.cursor()
    arch_cur.execute("select children from physicalcomponents where cname='" + component + "'")
    children = []
    depth-=1
    result = arch_cur.fetchone()
    if result != None:
        result_children = result[0]
        if result_children != None:
            childrenlist = result_children.split(",")
            children += childrenlist
            if depth > 0:
                for child in childrenlist:
                    children += get_children(child, depth)
    return children


def get_relations(component, depth):
    # get parents
    comps_parents = get_parents(component, depth)
    print("parents: " + str(comps_parents))
    # get children
    comps_children = get_children(component, depth)
    print("children: " + str(comps_children))
    # make unique
    related = comps_parents + comps_children
    # return the list of all related components
    return related


def print_anns(anns_out, headers, format=None):
    if format == 'table':
        for ann in anns_out:
            ann_str = '\t'.join(str(e) for e in ann)
            print(ann_str)

    elif format == 'json':
        outputlist = []
        for ann in anns_out:
            outputdict = {}
            for i in range (len(headers)-1):
                outputdict[headers[i]] = ann[i]
            outputlist.append(outputdict)
        print(json.dumps(outputlist))

    else:
        if len(anns_out) == 0:
            print('No annotations found')
        for ann in anns_out:
            print('----------')
            print('[' + str(ann[headers.index('id')]) + ']', 'by', ann[headers.index('authorid')], 'on system', ann[headers.index('system')])
            print('Time:', ann[headers.index('starttime')],'to', ann[headers.index('endtime')])
            print('Start state:', ann[headers.index('startstate')], '; End state:',  ann[headers.index('endstate')])
            print('Description:', ann[headers.index('description')])
            print('Manually invoked?', bool(ann[headers.index('manual')]), '; System down?: ',  bool(ann[headers.index('systemdown')]))
            print('Components:', ann[headers.index('components')])
            print('Tags:')
            if ann[headers.index('LDcatgroup')]:
                print ('LogDiver category group:', ann[headers.index('LDcatgroup')])
            if ann[headers.index('LDcategory')]:
                print ('LogDiver category:', ann[headers.index('LDcategory')])
            if ann[headers.index('LDtag')]:
                print ('LogDiver tag:', ann[headers.index('LDtag')])
            if ann[headers.index('balerpatternid')]:
                print ('Baler pattern ID:', ann[headers.index('balerpatternid')])
            print('Relevant log files:', ann[headers.index('logfiles')])


def main():
    parser = argparse.ArgumentParser(description="Query the annotations for a logset.")
    parser.add_argument("output", choices=["annotations", "logs", "a", "l"], metavar="output", help="specifier to return annotations or log lines")
    parser.add_argument("-s", "--starting", help="a start time that limits retrieved annotations or log lines to those relevant to the time at or after it, e.g. '2017-01-01 12:00:00'. When used in conjunction with --jobid, overrides the job start time.")
    parser.add_argument("-e", "--ending", help="an end time that limits retrieved annotations or log lines to those relevant to the time at or before it, e.g. '2017-01-01 12:59:59'. When used in conjunction with --jobid, overrides the job end time.")
    parser.add_argument('-c', '--component', help="a component identifier. When used in conjunction with --jobid, overrides the job's list of components.")
    parser.add_argument('-t', '--type', help="a string describing a type of event")
    parser.add_argument('-a', '--after', help="a time after which to find the next single example that otherwise matches the query, e.g. '2017-01-01 12:00:00'")
    parser.add_argument('-l', '--logseries', help="a single series of log files in which to find log lines")
    parser.add_argument('-j', '--jobid', help="a job id from the scheduler")
    parser.add_argument('-f', '--format', help="how to format the output, table, json, or default")
    parser.add_argument('-d', '--depth', help="how many levels to traverse to find related components. This has no effect without a component or job flag.")
    parser.add_argument('-v', '--verbose', help="be noisier. -vv is noisier than -v, etc", action='count')
    parser.add_argument('--jobs', help="show which jobs were running during the time spanned by this annotation id")
    parser.add_argument('--limit', help="limit the number of output records")
    args = parser.parse_args()

    anns_or_logs = args.output
    where_clause = ""
    order_clause = "order by starttime"
    limit_clause = ""
    componentlist = []
    timeformat = '%Y-%m-%d %H:%M:%S'

    if args.verbose:
        logger = logging.getLogger()
        level = logger.getEffectiveLevel() - 10*args.verbose
        logger.setLevel(level)

    if args.limit:
        limit_clause = ' limit {0} '.format(args.limit)


    # if a jobid is given, get the nodes, start and end for the job, then set the
    # component, start, and end parameters.
    if args.jobid:
        try:
            jobs_connn = sqlite3.connect(jobdb)
            jobs_cur = jobs_connn.cursor()
            jobs_cur.execute("select * from jobs where jobid=" + args.jobid)
        except:
            print("Unable to connect to the jobs database.")
            return

        jobs_column_names = [e[0] for e in jobs_cur.description]
        jobinfo = jobs_cur.fetchone()
        if jobinfo != None:
            started = jobinfo[jobs_column_names.index('Start')]
            finished = jobinfo[jobs_column_names.index('End')]
            nodeliststr = jobinfo[jobs_column_names.index('NodeList')]
            if not re.match('\[\"c.*\"', nodeliststr):
                if 'nid' in nodeliststr:
                    nodeliststr = expand_nidlist(nodeliststr)
                else:
                    nodeliststr = expand_numlist(nodeliststr)
            print('Job {} started at {} and finished at {}.'.format(args.jobid, started, finished))
            print('It ran on the following nodes: {}'.format(nodeliststr))
            #use the start and end time of the job unless they were given by the user
            if not args.starting:
                args.starting = started # when the job started
            if not args.ending:
                args.ending = finished # when the job ended
            if not args.component:
                # ignore component list if user specified a single component
                componentlist = json.loads(nodeliststr)
        else:
            print('No job found.')
            return

    # hmm, do we in fact having any overlapping jobs and annotations?
    #TODO make this work for more than one author's set of IDs
    if args.jobs:
        ann_ids = args.jobs.split(',')
        logging.debug(args)
        where_clause = ' where id in ( ' + ','.join(['?' for i in ann_ids]) + ' )'
        if 'all' in ann_ids:
            # get everything!
            where_clause = ''
            ann_ids = []
        
        ann_db = sqlite3.connect(annodb)
        with ann_db:
            cursor = ann_db.cursor()
                        
            prefix = 'select * from annotations'
            #suffix = ' order by starttime'
            #query = prefix + where + suffix
            query = "{0} {1} {2} {3}".format(prefix,where_clause,order_clause,limit_clause)
            logging.debug("running query: {0}".format(query))
            try:
                cursor.execute(query, ann_ids)
            except:
                print("Unable to connect to the annotations database.")
                return
            anns_found = cursor.fetchall()
            ann_column_names = [i[0] for i in cursor.description]
            #print_anns(anns_found, ann_column_names, args.format)
        logging.debug("found {0:d} annotations".format(len(anns_found)))
        # ugh, we have to do essentially a cross-database join on overlapping 
        # date ranges, not pretty.
        # There's a sort-of-elegant solution via 
        # "attach database 'jobs.sqlite3' as jobs;" and then make a temporary
        # table of data ranges (see 
        # https://stackoverflow.com/questions/15075717/join-overlapping-date-ranges
        # for some ideas)
        # for now we'll go the quick-and-dirty route of a query per annotation:
        limit_clause = ' limit 50 '
        order_clause = ' order by start '
        prefix = 'select * from jobs'
        jobs_db = sqlite3.connect(jobdb)
        with jobs_db:
            cursor = jobs_db.cursor()
            prefix = 'select JobID, UID, JobName, NNodes, Start, End from jobs '
            count = 0
            for ann in anns_found:
                count += 1
                if count%100 == 0:
                    logging.info("checked {0:d} jobs".format(count))
                start = ann[ann_column_names.index('starttime')]
                end = ann[ann_column_names.index('endtime')]
                system = ann[ann_column_names.index('system')]
                # the limit here is because bad entries in db lead to zillions of results printed
                # (see annotation id 870091)
                where_clause = ' where system=? and Start<=? and End>=? '
                query = "{0} {1} {2} {3}".format(prefix,where_clause,order_clause,limit_clause)
                logging.debug("running query: {0}".format(query))
                cursor.execute(query, [system, end, start])
                jobs_found = cursor.fetchall()
                if len(jobs_found)>0:
                    print_anns([ann], ann_column_names, 'table')
                    for j in jobs_found:
                        print(j)
        return

    # if a start time is given, find annotations with time ranges that overlap the time
    # after it. That is, with end times that are after it (so we catch stuff that started
    # before but ended after.)
    if args.starting:
        # try to guess the time format and convert it to what SQLite3 understands
        starting_datetime = dateparser.parse(args.starting)
        formatted_starting = starting_datetime.strftime(timeformat)
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        where_clause += " endtime >= '" + formatted_starting + "'"

    # if an end time is given, find annotations with time ranges that overlap the time
    # before it. That is, with start times that are before it (so we catch stuff that
    # started before and ended after.)
    if args.ending:
        ending_datetime = dateparser.parse(args.ending)
        formatted_ending = ending_datetime.strftime(timeformat)
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        where_clause += " starttime <= '" + formatted_ending + "'"

    # if both a start and end are given, find annotations whose time ranges overlap them.
    # This is already handled above. That is, we want annotations with start times before
    # the end time and end times after the start time.


    # if a component is given, query for annotations that apply to that component.
    if args.component and not args.depth:
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        where_clause += " components like '%\"{0}\"%' ".format(args.component)

    # if a component and levels are given, query for the component and each related component
    if args.component and args.depth:
        componentlist = [args.component]
        componentlist += get_relations(args.component, int(args.depth))

    # if a type of event is specified, query for annotations with the phrase in
    # their description or endstate or a logdiver tag
    if args.type:
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        col,sep,pattern = args.type.lower().rpartition('=')
        cols = ('description', 'endstate', 'ldcatgroup', 'ldcategory', 'ldtag')
        cols_short = [c[:3] for c in cols]
        if col[:3] in cols_short:
            # find the actual column it matches
            for c in range(len(cols)):
                if cols[c].startswith(col):
                    actual_col = cols[c]
                    break
            else:
                raise Exception("can't find a column matching {0}".format(col))
            where_clause += " {0} like '%{1}%' ".format(actual_col,pattern)
        else:
            where_clause += " ( " + " or ".join([" {0} like '%{1}%' ".format(col,args.type) for col in cols]) + " ) "
        #where_clause += " (description like '%" + args.type + "%' or endstate like '%" + args.type + "%' or ldtag like '%" + args.type + "%')"

    # if an "after" timestamp is given, set the where clause to use it as a lower
    # bound on the start time, and only retrieve the first one after that time.
    # This finds only the first instance that *started* after the time given.
    if args.after:
        after_datetime = dateparser.parse(args.after)
        formatted_after = after_datetime.strftime(timeformat)
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        limit_clause = 'limit 1'
        where_clause += " starttime > '" + formatted_after # + "' order by starttime limit 1"

    #logfile requests - not working yet
    #if a logseries is given, only look for logs in that series
    if args.logseries:
        pass

    # test the db connection
    try:
        anns_connn = sqlite3.connect(annodb)
        anns_cur = anns_connn.cursor()
        anns_cur.execute('select * from annotations limit 1')
    except:
        print("Unable to connect to the annotations database.")
        return


    # get names of columns for handling annotation query results,
    ann_column_names = [i[0] for i in anns_cur.description]
    headerstr = '\t'.join(ann_column_names)
    if args.format=='table':
        print(headerstr)

# do this:
# sqlite3 jobs.sqlite3 "create index starttime on jobs (start asc) ;"
# sqlite3 jobs.sqlite3 "create index endtime on jobs (end asc) ;"
# sqlite3 annotations.sqlite3 "create index start on annotations (starttime asc) ;"
# sqlite3 annotations.sqlite3 "create index end on annotations (endtime asc) ;"


    # run the queries
    prefix = 'select * from annotations'
    #suffix = ' order by starttime'
    if len(componentlist) > 0:
        # run a query for each component, if there are more than one. 
        # We don't run them all into a single query because they can easily 
        # exceed the limit of query expressions.
        comp_count=0
        for component in componentlist:
            comp_count+=1
            if comp_count%100 == 0:
                print("checked " + str(comp_count) + " components")
            new_clause = where_clause
            if new_clause != "":
               new_clause += " and"
            else:
               new_clause += " where"
            new_clause += " components like '%\"{0}\"%' ".format(component)
            query = "{0} {1} {2} {3}".format(prefix,new_clause,order_clause,limit_clause)
            logging.debug("Executing query: {0}".format(query))
            anns_cur.execute(query)
            anns_found = anns_cur.fetchall()
            if len(anns_found) > 0:
               print(component)
               print_anns(anns_found, ann_column_names, args.format)
        print("*** Done! ***")

    else:
        # run one query on the db
        #query = prefix + where_clause + suffix
        query = "{0} {1} {2} {3}".format(prefix,where_clause,order_clause,limit_clause)
        logging.debug("Executing query: {0}".format(query))
        anns_cur.execute(query)
        anns_found = anns_cur.fetchall()
        print_anns(anns_found, ann_column_names, args.format)

main()
