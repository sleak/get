# Get
A small python app to serve as a query engine for resilience annotations.
Needs to be in a directory with annotations.sqlite3 and jobs.sqlite3 databases.

examples
--------
annotations during a time range
./get.py annotations -s '2018-03-20 00:00:00' -e '2018-03-20 11:00:00'

annotations about a component
./get.py annotations -c 'nid01681'

annotations about a type of issue
./get.py annotations -t 'node down'

combinations of the above
./get.py annotations -t 'node down' -s '2018-03-20 00:00:00' -e '2018-03-20 01:00:00'

the next annotation after a certain time point about whatever you like
./get.py annotations -t 'node down' -a '2018-03-20 11:51:00'

annotations related to a job by its id
./get.py annotations -j 9847125

all the above in default format
./get.py annotations -j 9847125
. . . in json format
./get.py annotations -j 9847125 -f json
. . . in tabular format
./get.py annotations -j 9847125 -f table

